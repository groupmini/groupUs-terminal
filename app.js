'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf8');
require('dotenv').config()
const ACCESS_TOKEN = process.env.at;

var util = require('util');
var API = require('groupme').Stateless
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var querystring = require('querystring');
var http = require('http');
var fs = require('fs');
var HTTPS = require('https');
var request = require('request');



var askValue = 0;


var chatNameRule = [];

function sleep(ms) { // number of milliseconds to sleep
    return new Promise(function (resolve) { // creates a new promise
        setTimeout(resolve, ms); // promise will be resolved when the timeout completes
    });
}


var chats = JSON.parse(httpGet("https://api.groupme.com/v3/groups?token=" + ACCESS_TOKEN));

var value;
var last_command;
var sendtowho;

process.stdin.on('data', async function (chunk) {
    if (chunk == "get chats\r\n") {
        for (var i = 0; i < chats.response.length; i++) {
            console.log(chats.response[i].name);
        }
        last_command = 1;
    } 
    else if (chunk == "pick chat\r\n") {
        console.log("Pick a chat!");
        for (var i = 0; i < chats.response.length; i++) {
            console.log(i + ": " + chats.response[i].name);
        }
        last_command = 2;
    }    
    else if (last_command > 0) {
        if (last_command == 2) {
            var groupid = chunk;
            console.log("Group ID is " + groupid);
            console.log("What do you want to say?");
            last_command = 3;
        }
        else if (last_command == 3) {
            console.log("Sending " + chunk);
            var MessageData = {
                message: {
                    source_guid: "383-484-395", text: chunk
                }
            }
            console.log("GroupID is " + groupid);
            API.Messages.create(ACCESS_TOKEN, chats.response[0].id, MessageData, function (err, ret) {
                if (!err) {
                    console.log("Message sent!");
                }
            });

        }
    }
    else {
        last_command = 0;
        }
});


function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}